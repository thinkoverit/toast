

function makeText(text, duration) {

    "use strict";

    if (typeof (text) !== "string") {
        throw new Error("The `text` parameter is missing.");
    };

    var d = (typeof (duration) === "string" && duration[0] === "l") ? 3.5 : 2;

    var toast = iToast.makeText(text);
    return toast.show(iToastTypeNotice);

};
 
 
exports.makeText = makeText;